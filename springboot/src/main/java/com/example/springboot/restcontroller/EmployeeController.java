package com.example.springboot.restcontroller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.entity.Employee;
import com.example.springboot.exception.EmployeeException;
import com.example.springboot.service.EmployeeService;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public List<Employee> findCities() {

        List<Employee> employees = employeeService.retrieveEmployees();
        return employees;
    }

    @GetMapping("/{employeeId}")
    public Employee findEmployee(@PathVariable Long employeeId) {
        Employee emp = employeeService.getEmployee(employeeId);
        if (emp == null) {
            throw new EmployeeException(employeeId);
        }
        return employeeService.getEmployee(employeeId);
    }

    @PostMapping
    public Employee saveEmployee(Employee employee){
        return employeeService.saveEmployee(employee);
    }

    @DeleteMapping("/{employeeId}")
    public HashMap<String, String> deleteEmployee(@PathVariable(name="employeeId")Long employeeId){
        Employee emp = employeeService.getEmployee(employeeId);
        if (emp == null) {
            throw new EmployeeException(employeeId);
        }
        employeeService.deleteEmployee(employeeId);
        HashMap<String, String> map = new HashMap<>();
        map.put("message", "Delete Successfully!");
        return map;
    }

    @PutMapping("/{employeeId}")
    public Employee updateEmployee(@RequestBody Employee employee,
                               @PathVariable(name="employeeId")Long employeeId){
        Employee emp = employeeService.getEmployee(employeeId);
        if(emp != null){
            if ( employee.getEmail() != null ) {
                emp.setEmail(employee.getEmail());
            }
            if ( employee.getName() != null ) {
                emp.setName(employee.getName());
            }
            if ( employee.getRole() != null ) {
                emp.setRole(employee.getRole());
            }
            employeeService.updateEmployee(emp);
            return emp;
        }
        else {
            employee.setId(employeeId);
            employeeService.updateEmployee(employee);
            return employee;
        }
    }
}
