# spring-boot-training
- Database: H2 Database
    - 
- APIs:
    ---
    - GET http://localhost:8080/api/employees: return employee list
    - GET http://localhost:8080/api/employees/{employeeId}: return an employee by Id
    - POST http://localhost:8080/api/employees: save new employee
           
           - Content-Type: multipart/form-data; boundary=<calculated when request is sent>
           - Body(form-data): name, email, role
           
    - PUT http://localhost:8080/api/employees/{employeeId}: update an employee base on employeeId
    
           - Content-Type: application/json
           - Body: {
                       "name": "abc1",
                       "email": "abc@gmail.com",
                       "role": "member"
                   }
                   
    - DELETE http://localhost:8080/api/employees/{employeeId}: delete an employee base on employeeId

